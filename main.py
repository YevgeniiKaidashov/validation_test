from validators import RequiredValidator, IntegerValidator, StringValidator, MinValidator
from forms import Field, BaseForm


class UserForm(BaseForm):
    name = Field(
        field_name='name',
        validators=(RequiredValidator, StringValidator)
    )
    surname = Field(
        field_name='surname',
        validators=(RequiredValidator,)
    )
    age = Field(
        field_name='age',
        validators=(RequiredValidator, IntegerValidator, MinValidator)
    )

    def __init__(self, name=None, surname=None, age=None, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.name = name
        self.surname = surname
        self.age = age

    def save(self):
        print 'Save model after validation'


##############################################
line = '=' * 10 + ' First case ' + '=' * 10
print line
test = UserForm(name="Yevgenii", surname="Kaidashov", age=10)

if not test.is_valid():
    print test.errors
else:
    test.save()

print '=' * len(line)

###############################################
line = '=' * 10 + ' Second case ' + '=' * 10
print line
test = UserForm(surname="Kaidashov", age=10)

if not test.is_valid():
    print test.errors
else:
    test.save()

print '=' * len(line)

###############################################
line = '=' * 10 + ' Third case ' + '=' * 10
print line
test = UserForm(name=1212, age=18)

if not test.is_valid():
    print test.errors
else:
    test.save()

print '=' * len(line)

###############################################
line = '=' * 10 + ' Fourth case ' + '=' * 10
print line
test = UserForm(name="Yevgenii", surname="Kaidashov", age=27)

if not test.is_valid():
    print test.errors
else:
    test.save()

print '=' * len(line)
