import cerberus


class BaseValidator(object):
    field_name = None
    validation_schema = None

    def __init__(self):
        self._validator = cerberus.Validator()

    def __call__(self, value):
        document = {self.field_name: value}
        schema = {self.field_name: self.validation_schema}
        if not self._validator.validate(document, schema):
            return self._validator.errors
        return None


class RequiredValidator(BaseValidator):
    validation_schema = {
        'required': True
    }


class IntegerValidator(BaseValidator):
    validation_schema = {
        'type': 'integer'
    }


class StringValidator(BaseValidator):
    validation_schema = {
        'type': 'string'
    }


class MinValidator(BaseValidator):
    validation_schema = {
        'min': 18
    }
