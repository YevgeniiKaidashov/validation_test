class Field(object):
    def __init__(self, field_name, validators=None):
        self._value = None
        self.field_name = field_name
        self.validators = validators or []

    def __set__(self, instance, value):
        self._value = value

    @property
    def value(self):
        return self._value


class BaseForm(object):
    def __init__(self, data=None):
        self.data = data or {}
        self._errors = None

    def is_valid(self):
        return not self.errors

    def _validate(self):
        errors = {}

        for field in self.__class__.__dict__.values():
            if not isinstance(field, Field):
                continue
            for validator in field.validators:
                v = validator()
                v.field_name = field.field_name
                error = v(field.value)
                if error:
                    if field.field_name in errors:
                        errors[field.field_name].append(error[field.field_name][0])
                    else:
                        errors[field.field_name] = [error[field.field_name][0]]

        self._errors = errors or None

    @property
    def errors(self):
        if self._errors is None:
            self._validate()
        return self._errors
